#include <iostream>

int main() {
  int i = 5;
  int j = 2;
  auto result = [](const int& first, const int& second) -> int {
    return first + second;
  };
  std::cout << result(i, j) << std::endl;
  auto text = []() { std::cout << "lambda" << std::endl; };
  text();
  [&i, &j]() mutable {
    ++i;
    ++j;
  }();
  std::cout << i << " " << j << std::endl;
}
